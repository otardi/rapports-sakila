<?php

require 'functions.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$sql=getQuery("req.sql","CATEGORIE"); 
$mysqli = mysqli_connect('localhost', 'app', 'abc-123', 'sakila');

if ($mysqli->connect_errno) {

    echo "Proublem:";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    exit;
}
if (!$result = $mysqli->query($sql)) {
    echo "Sorry, the website is experiencing problems.\n";
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}
/* ECHO la table au format JSON */
echo '{"cols": [{"id":"","label":"CATÉGORIE","pattern":"","type":"string"},{"id":"","label":"NOMBRE","pattern":"","type":"number"}],"rows": [';
$rows = array();
while ($emp = $result->fetch_array()) {
    $cat = $emp[0];
    $nb = $emp[1];
    array_push($rows,'{"c":[{"v":"' . $cat . '","f":null},{"v":' . $nb . ',"f":null}]}');
    
}
$conc=implode(",",$rows);
echo $conc . "]}";

$result->free();
$mysqli->close();

?>

