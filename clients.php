<?php
require 'head.php';
require 'functions.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$sql=getQuery("req.sql","CLIENTS"); 
$mysqli = mysqli_connect('localhost', 'app', 'abc-123', 'sakila');

if ($mysqli->connect_errno) {

    echo "Proublem:";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    exit;
}
if (!$result = $mysqli->query($sql)) {
    echo "Sorry, the website is experiencing problems.\n";
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}
echo '<table id="example" class="display" style="width:100%"><thead><th>NOM</th><th>PRÉNOM</th><th>ADRESSE</th><th>TÉL</th><th>VILLE</th></thead>';
while ($emp = $result->fetch_array()) {
	echo "<tr>";
    echo '<td>' . $emp[0] . '</td><td>' . $emp[1] . '</td><td>' . $emp[2] . '</td><td>' . $emp[3] . '</td><td>' . $emp[4] . '</td>';
    echo "</tr>\n";
}
echo "</table>\n";

$result->free();
$mysqli->close();

?>


    </body>
</html> 