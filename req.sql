EMPLOYES:SELECT staff.last_name, staff.first_name, address.phone FROM staff JOIN address ON address.address_id = staff.address_id
MAGASINS:SELECT address, city, phone FROM address JOIN city ON address.city_id = city.city_id JOIN store ON store.address_id = address.address_id
CLIENTS:SELECT last_name, first_name, address, phone, city FROM address JOIN city ON address.city_id = city.city_id JOIN customer ON customer.address_id = address.address_id
INVENTAIRE:SELECT title, city, count(*) FROM film JOIN inventory ON inventory.film_id = film.film_id JOIN store ON store.store_id = inventory.store_id JOIN address ON store.address_id = address.address_id JOIN city ON city.city_id = address.city_id GROUP BY title, city
CATEGORIE:SELECT category.name, count(*) FROM category JOIN film_category ON film_category.category_id = category.category_id JOIN film ON film.film_id = film_category.film_id GROUP BY category.name
